package com.zenoti.api.config;

public class QueryConstants {
	public static final int MAX_RESULT = 4;
	
	public interface ConstantNames{
		public static final String PAGE = "page";
	}
}
