package com.zenoti.api.controller;
/* IMPORTANT: Multiple classes and nested static classes are supported */

//imports for BufferedReader
import java.io.BufferedReader;
import java.io.InputStreamReader;

//import for Scanner and other utility  classes
import java.util.*;

class TestClass {
  public static void main(String args[] ) throws Exception {
     Scanner sc = new Scanner(System.in);
		// your code goes here
		int T;
		T = sc.nextInt();
		for(int i=0;i<T;i++){
			int N,M,sX,sY,mJ;
			N = sc.nextInt();
			M = sc.nextInt();
			
			int [][] bV = new int[N][];
			boolean [][]Visit = new boolean[N][];
			for(int c=0;c<N;c++){
				bV[c]=new int[M];
				Visit[c] = new boolean[M];
			}
			for(int r=0;r<N;r++){
				for(int c=0;c<M;c++){
					bV[r][c] = sc.nextInt();
					Visit[r][c] = false;
				}
			}
			sX = sc.nextInt();
			sY = sc.nextInt();
			mJ = sc.nextInt();
			// do recursive function
			ArrayList<Integer>xPath = new ArrayList<Integer>();
			ArrayList<Integer>yPath = new ArrayList<Integer>();
			
			boolean isFinal = toEdge(bV,Visit,xPath,yPath,sX-1,sY-1,N,M,mJ);
			if(!isFinal){
				System.out.println("NO");
			}else{
				System.out.println("YES");
				System.out.println(xPath.size());
				for(int k = 0; k < xPath.size(); k++) {   
				    System.out.println(xPath.get(k)+" "+yPath.get(k));
				}
			}
			 
		}
  }
  
  public static boolean toEdge(int[][]bV,boolean[][]Visit,ArrayList<Integer>xPath,ArrayList<Integer>yPath,int cX,int cY,int R,int C,int mJ){
		
		boolean isGood = false;
		// push these coordinates
		//System.out.println("next place is "+cX+" "+cY);
		xPath.add(cX+1);
		yPath.add(cY+1);
		Visit[cY][cX] = true;
		//System.out.println("on "+cX+" "+cY);
		// check boundryCond
		if(cX == 0 || cX == C-1 || cY==0 || cY==R-1){
			//System.out.println("returning from "+cX+" "+cY);
			return true;	
		}
		// 4 condition
		// cX, cY-1
		
		if(cY>0 && !Visit[cY-1][cX] &&(bV[cY][cX]-bV[cY-1][cX])>=0 && (bV[cY][cX]-bV[cY-1][cX])<=mJ){
			//System.out.println("going to place ^ "+(bV[cX][cY-1])+" "+cX+" "+(cY-1));
			isGood = toEdge(bV,Visit,xPath,yPath,cX,cY-1,R,C,mJ);
			if(isGood)return true;
		}
		// cX, cY+1
		if(cY<R-1 && !Visit[cY+1][cX]&&(bV[cY][cX]-bV[cY+1][cX])>=0 && (bV[cY][cX]-bV[cY+1][cX])<=mJ){
			//System.out.println("going to place V "+cX+" "+(cY+1));
			isGood = toEdge(bV,Visit,xPath,yPath,cX,cY+1,R,C,mJ);
			if(isGood)return true;
		}
		// cX-1,cY
		if(cX>0 && !Visit[cY][cX-1]&&(bV[cY][cX]-bV[cY][cX-1])>=0 && (bV[cY][cX]-bV[cY][cX-1])<=mJ){
			//System.out.println("going to place < "+(cX-1)+" "+cY);
			isGood = toEdge(bV,Visit,xPath,yPath,cX-1,cY,R,C,mJ);
			if(isGood)return true;
		}
		// cX+1,cY
		if(cX>C-1 && !Visit[cY][cX+1]&&(bV[cY][cX]-bV[cY][cX+1])>=0 && (bV[cX][cY]-bV[cY][cX+1])<=mJ){
			//System.out.println("going to place > "+(cX+1)+" "+cY);
			isGood = toEdge(bV,Visit,xPath,yPath,cX+1,cY,R,C,mJ);
			if(isGood)return true;
		}
		//System.out.println("returning from "+(cX)+" "+cY);

		// backTrack
		xPath.remove(xPath.size()-1);
		yPath.remove(yPath.size()-1);
		Visit[cY][cX] = false;
		return false;
	}
}
