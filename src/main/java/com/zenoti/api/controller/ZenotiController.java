package com.zenoti.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zenoti.api.service.ZenotiDataService;
import com.zenoti.model.ZenotiData;

@RestController
public class ZenotiController {
	
	@Autowired
	ZenotiDataService zenotiDataService;
	
	@RequestMapping(value = "/getAllZenotiData", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<ZenotiData> getZenotiData() {
		List<ZenotiData> listOfZenotiData = zenotiDataService.getAllZenotiData();
		return listOfZenotiData;
	}

	@RequestMapping(value = "/getZenotiDataById/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ZenotiData getZenotiDataById(@PathVariable Integer id) {
		return zenotiDataService.getZenotiData(id);
	}

	@RequestMapping(value = "/addZenotiData", method = RequestMethod.POST, headers = "Accept=application/json")
	public void addZenotiData(@RequestBody ZenotiData ZenotiData) {	
		zenotiDataService.addZenotiData(ZenotiData);
		
	}

	@RequestMapping(value = "/updateZenotiData", method = RequestMethod.PUT, headers = "Accept=application/json")
	public void updateZenotiData(@RequestBody ZenotiData ZenotiData) {
		zenotiDataService.updateZenotiData(ZenotiData);
	}

	@RequestMapping(value = "/deleteZenotiData/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public void deleteZenotiData(@PathVariable("id") Integer id) {
		zenotiDataService.deleteZenotiData(getZenotiDataById(id));		
	}	
}
