package com.zenoti.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zenoti.api.service.ZenotiDataService;
import com.zenoti.model.ZenotiData;

@RestController
public class ZenotiSearchController {
	
	@Autowired
	ZenotiDataService zenotiDataService;
	

	@RequestMapping(value = "/searchZenotiData", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<ZenotiData> searchZenotiData(@RequestParam("query") String queryString,@RequestParam(value="pageNo",required=false) Integer pageNo,@RequestParam(value="maxResult",required=false) Integer maxResult) {
		List<ZenotiData> listOfZenotiData = zenotiDataService.searchZenotiData(queryString,pageNo,maxResult);
		return listOfZenotiData;
	}
	
}
