package com.zenoti.api.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Service;

import com.zenoti.model.ZenotiData;


/**
 * @author <a href="mailto:psunil1278@gmail.com">Sunil Kumar</a>
 * @since 26/12/15
 */
@Service
public class ZenotiDataDAO implements GenericDao<ZenotiData,Integer>{

    @Autowired
    private HibernateTemplate hibernateTemplate;


    @Override
    public ZenotiData get(final Integer id) {
        return hibernateTemplate.get(ZenotiData.class,id);
    }

    @Override
    public List<ZenotiData> getAll() {
        return hibernateTemplate.loadAll(ZenotiData.class);
    }

    public List<ZenotiData> searchByQuery(String queryString,Integer pageNo,Integer maxResult) {
    	Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		Criteria cr = session.createCriteria(ZenotiData.class);
		cr.add(Restrictions.or(Restrictions.like("business_name", "%"+queryString+"%"),
				Restrictions.like("business_type", "%"+queryString+"%")));
		if(pageNo != null){
			if(maxResult!=null){
				cr.setFirstResult(pageNo*maxResult);
			}	
			
		}
		if(maxResult!=null){
			cr.setMaxResults(maxResult);
		}
		cr.addOrder(Order.desc("rating"));
		return cr.list();
    }
    
    @Override
    public void save(final ZenotiData object) {
         hibernateTemplate.save(object);
    }

    @Override
    public void saveOrUpdate(final ZenotiData object) {
        hibernateTemplate.saveOrUpdate(object);
    }

    @Override
    public void delete(final ZenotiData object) {
        hibernateTemplate.delete(object);
    }

    @Override
    public Long count() {
        return new Long(hibernateTemplate.loadAll(ZenotiData.class).size());
    }

    @Override
    public void flush() {
        hibernateTemplate.flush();
    }
}
