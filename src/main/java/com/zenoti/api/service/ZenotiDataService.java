package com.zenoti.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zenoti.api.dao.ZenotiDataDAO;
import com.zenoti.model.ZenotiData;

@Service("zenotiDataService")
@Transactional
public class ZenotiDataService {

	@Autowired
	ZenotiDataDAO zenotiDataDao;
	
	
	public List<ZenotiData> getAllZenotiData() {
		return zenotiDataDao.getAll();
	}
	public List<ZenotiData> searchZenotiData(String queryString,Integer pageNo,Integer maxResult) {
		return zenotiDataDao.searchByQuery(queryString,pageNo,maxResult);
	}
	
	public ZenotiData getZenotiData(Integer id) {
		return zenotiDataDao.get(id);
	}
	
	public void addZenotiData(ZenotiData zenotiData) {
		zenotiDataDao.save(zenotiData);
	}

	public void updateZenotiData(ZenotiData ZenotiData) {
		zenotiDataDao.saveOrUpdate(ZenotiData);

	}

	public void deleteZenotiData(ZenotiData zenotiData) {
		zenotiDataDao.delete(zenotiData);
	}
}
