package com.zenoti.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Data
@Entity
@Table(name = "zenoti_data")
public class ZenotiData implements Serializable{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "business_name")
	private String business_name;
	
	@Column(name = "business_type")
	private String business_type;
	
	@Column(name = "link")
	private String link;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "rating")
	private BigDecimal rating;
	
}
